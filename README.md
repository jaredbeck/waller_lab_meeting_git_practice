# Repository Information #

### What is this repository for? ###

* This repository is designed to help the Waller Lab learn how to use Git!

### How do I get started? ###

* Install [R](https://cran.r-project.org/) 
* Install [R Studio](https://www.rstudio.com/)
* Install [Git](https://git-scm.com/)

### How do I set up Git on my computer? ###

* After installing Git, open the Git terminal and type:
    + git config --global user.name “[user name]“
    + git config --global user.email “[email address]"
* To make sure R Studio plays nice with Git:
    + Open R Studio 
    + Click Tools --> Global Options --> Version Control --> Git/SVN 
    + Ensure the file path to Git is correct

### For more information? ###

* Here is some good [background information](http://r-bio.github.io/intro-git-rstudio/) about using Git with R Studio
* Here is a helpful [tutorial](https://jennybc.github.io/2014-05-12-ubc/ubc-r/session03_git.html) 
* [Setting up Git with GitHub](https://help.github.com/articles/set-up-git/)
* [Setting up Git with R Studio](https://support.rstudio.com/hc/en-us/articles/200532077-Version-Control-with-Git-and-SVN)
* [Practice using Git commands](https://try.github.io/) with Octokitty!
* [Advanced guide to using Git](https://git-scm.com/book/en/v2)

### Who do I talk to? ###

* Contact Jared with any questions!