---
title: "Deer Exclosures and Functional Diversity"
author: "Ali Paulson"
date: "3/1/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, fig.height = 8, fig.width = 4)
library(FD)
library(plyr)
library(dplyr)
library(reshape2)
library(ggplot2)
library(tidyr)
#library(devtools)
#install_github("daijiang/hillR")
library(hillR)
options(scipen = 9999, digits = 3)
```

```{r bring in and organize data}
####Bring in functional trait data####
#This dataset contains all spp from my exclosure dataset, whether or
  #not they are in the trait dataset (made with PrepTraits.Rmd and 
  #Spp.to.traits.R).  I have log transformed the highly skewed traits
  #(e.g. Seed Mass, log vegetative height, etc.).  
func.pel = read.csv("../CleanData/log.deerfuncpel.csv", stringsAsFactors = FALSE, row.names = 1)
#names(func.pel)

#Going to reduce this functional trait database a bit - I have a lot 
  #of NAs, and they are screwing up the diversity measurements (e.g.
  #spp that have NAs across the board).  I may be able to include
  #some of these spp, but I just want to get a first effort at these 
  #diversity measurements, using a fairly complete dataset.  

func.pel.red <- func.pel %>% 
  filter(!is.na(Taxon)) %>% #Goes from 220 spp to 128 spp!
  select(-Taxon, -Taxon.Michigan.Flora) %>% #Remove non-traits
  select(-c(Life.Cycle, Lifeform, Circularity, LMgC, LCaC, LKC, log.Leaf.length, log.leaf.width, L.NDF, L.ADF, L.lignin, LPC))
##**May need to consider removing very low coverage traits**

#Make spp names the row names
row.names(func.pel.red) <- func.pel.red$Taxon.USDA
func.pel.red <- func.pel.red[ , -1]


#Scale the quantitative traits - to have mean 0 and variance 1
#names(func.pel.red)
trait.scale = scale(func.pel.red[ , 5:13], center=TRUE, scale=TRUE)
trait.scale = as.data.frame(trait.scale)

#Combine scaled traits with categorical traits
func.red.scale <- func.pel.red
func.red.scale[ , 5:13] <- trait.scale

#So, functionaldeerscale is a data frame of the 126 spp found in exclosures that 
#we have func. that we have good coverage for
#want to see if the code works first (didn't before).




####Bring in site x spp matrix for exclosures####
deer <- read.csv("../CleanData/ex.SiteBySpecies.20.csv", check.names = FALSE, row.names = 1)
#sum(colSums(deer)) #6172

a <- setdiff(names(deer), row.names(func.pel.red)) #88 sp different (correct)

deer.red <- deer %>% 
  select(-one_of(a)) #now 132 spp

#union(names(deer.red), row.names(func.pel.red)) #all there, 132

#What is my overall trait coverage (coarse - not trait by trait or
  #site level)?
#sum(colSums(deer.red)) #5484
# sum(colSums(deer.red))/sum(colSums(deer)) #88.9 % 

#88.9% is pretty good! But, it will be much lower at any given site/
  #trait combo.  Not going to investigate this now.  
```



###First, look at the functional diversity metrics from Villeger et al. 2008 / Laliberte et al. 2010. 

- fric = Functional richness (volume of space occupied by community - the convex hull volume)
- fdiv = Functional divergence, the distribution of abundance within the convex hull.  Tends towards 0 if the most abundant species are clumped at the center of the convex hull volume. 
- fdis = Functional dispersion, the mean distance in multidimensional space of individual speces to the community centroid
- feve = Functional evenness, the regularity of the distribution of abundance in the convex hull (regularity along a minimum spanning tree). Equals 1 if speicies spread is completely regular
- raoq = Rao's quadratic entropy

```{r test Villeger/Laliberte}
####Test FD with just quantitative traits of interest####
func.gow <- gowdis(func.red.scale) #note that scaling didn't do
  #anything...I'm a bit confused...I thought I needed to scale 
  #1st? 

test <- dbFD(x = as.matrix(func.gow), a = data.matrix(deer.red),
            calc.CWM = FALSE, messages = TRUE, print.pco = TRUE)
#summary(test)

deer.div <- data.frame(site = c(names(test$FRic)), 
                       fric = test$FRic, 
                       fdiv = test$FDiv, 
                       fdis = test$FDis, 
                       feve = test$FEve, 
                       raoq = test$RaoQ) %>% 
  melt(id.vars = "site", value.name = "div.value", variable.name = "div.measure") %>% 
  separate(site, into = c("exc_name", "exc"), sep = "_", remove = FALSE)
  

#Boxplot
deer.div %>% 
  ggplot(aes(x=exc, y=div.value))+
  geom_boxplot()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Diversity outside vs. inside exc.")

#See difference across fence
deer.div %>% 
  ggplot(aes(x=exc, y=div.value, group = exc_name))+
  geom_line()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Diversity outside vs. inside exc.")


#Just play with some ttests...but you are actually supposed to use
  #randomization tests to compare diversity values

deer.div %>% 
  mutate(div.measure = as.character(div.measure)) %>%
  group_by(div.measure, exc) %>% 
  summarise(value = list(div.value)) %>% 
  spread(exc, value) %>% 
  group_by(div.measure) %>% 
  mutate(p_value = t.test(unlist(no), unlist(yes), paired=TRUE)$p.value, 
         t_value = t.test(unlist(no), unlist(yes), paired=TRUE)$statistic)
```


###Next, look at the functional diversity metrics from Chao et al. 2014, with q=0.  

- Q = (Rao's Q)
- FDis = Functional dispersion
- D_q = functional hill number, the effective number of equally abundant and functionally equally distince species
- MD_q = mean functional diversity per species, the effective sum of pairwise distances between a fixed species and all other species
- FD_q = total functional diversity, the effective total functional distance between species of the assemblage


```{r Chao q0}
####Chao Div Metrics####
#Function spits out: 
# Q (Rao's Q)
# D_q (functional hill number, the effective number of equally abundant and functionally equally distince species)
# MD_q (mean functional diversity per species, the effective sum of pairwise distances between a fixed species and all other species)
# FD_q (total functional diversity, the effective total functional distance between species of the assemblage).


#For q=0, where spp abundance is not considered
hill_q0 <- as.data.frame(t(hill_func(comm = deer.red, traits = func.red.scale, q=0, div_by_sp = TRUE))) %>% 
  mutate(site = row.names(.)) %>% 
  melt(id.vars = "site", value.name = "div.value", variable.name = "div.measure") %>% 
  separate(site, into = c("exc_name", "exc"), sep = "_", remove = FALSE)

#Boxplot
hill_q0 %>% 
  ggplot(aes(x=exc, y=div.value))+
  geom_boxplot()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=0")

#See difference across fence
hill_q0 %>% 
  ggplot(aes(x=exc, y=div.value, group = exc_name))+
  geom_line()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=0")


#Just play with some ttests...but you are actually supposed to use
#randomization tests to compare diversity values

hill_q0 %>% 
  mutate(div.measure = as.character(div.measure)) %>%
  group_by(div.measure, exc) %>% 
  summarise(value = list(div.value)) %>% 
  spread(exc, value) %>% 
  group_by(div.measure) %>% 
  mutate(p_value = t.test(unlist(no), unlist(yes), paired=TRUE)$p.value, 
         t_value = t.test(unlist(no), unlist(yes), paired=TRUE)$statistic)
```


###Next look at the Chao et al. 2014 functional diversity metrics, with q=1. 


- Q = (Rao's Q)
- FDis = Functional dispersion
- D_q = functional hill number, the effective number of equally abundant and functionally equally distince species
- MD_q = mean functional diversity per species, the effective sum of pairwise distances between a fixed species and all other species
- FD_q = total functional diversity, the effective total functional distance between species of the assemblage


```{r q1}
#For q=1, where species are weighted in proportion to their frequency
hill_q1 <- as.data.frame(t(hill_func(comm = deer.red, traits = func.red.scale, q=1, div_by_sp = TRUE))) %>% 
  mutate(site = row.names(.)) %>% 
  melt(id.vars = "site", value.name = "div.value", variable.name = "div.measure") %>% 
  separate(site, into = c("exc_name", "exc"), sep = "_", remove = FALSE)

#Boxplot
hill_q1 %>% 
  ggplot(aes(x=exc, y=div.value))+
  geom_boxplot()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=1")

#See difference across fence
hill_q1 %>% 
  ggplot(aes(x=exc, y=div.value, group = exc_name))+
  geom_line()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=1")


#Just play with some ttests...but you are actually supposed to use
#randomization tests to compare diversity values

hill_q1 %>% 
  mutate(div.measure = as.character(div.measure)) %>%
  group_by(div.measure, exc) %>% 
  summarise(value = list(div.value)) %>% 
  spread(exc, value) %>% 
  group_by(div.measure) %>% 
  mutate(p_value = t.test(unlist(no), unlist(yes), paired=TRUE)$p.value, 
         t_value = t.test(unlist(no), unlist(yes), paired=TRUE)$statistic)
```


###Lastly, look at the funcitonal diversity metrics from Chao et al. 2014, where q=2. 


- Q = (Rao's Q)
- FDis = Functional dispersion
- D_q = functional hill number, the effective number of equally abundant and functionally equally distince species
- MD_q = mean functional diversity per species, the effective sum of pairwise distances between a fixed species and all other species
- FD_q = total functional diversity, the effective total functional distance between species of the assemblage

```{r Chao q3}
#For q=2, where abundant species are favored, rare species are discounted
hill_q2 <- as.data.frame(t(hill_func(comm = deer.red, traits = func.red.scale, q=2, div_by_sp = TRUE))) %>% 
  mutate(site = row.names(.)) %>% 
  melt(id.vars = "site", value.name = "div.value", variable.name = "div.measure") %>% 
  separate(site, into = c("exc_name", "exc"), sep = "_", remove = FALSE)

#Boxplot
hill_q2 %>% 
  ggplot(aes(x=exc, y=div.value))+
  geom_boxplot()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=2")

#See difference across fence
hill_q2 %>% 
  ggplot(aes(x=exc, y=div.value, group = exc_name))+
  geom_line()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=2")


#Just play with some ttests...but you are actually supposed to use
#randomization tests to compare diversity values

hill_q2 %>% 
  mutate(div.measure = as.character(div.measure)) %>%
  group_by(div.measure, exc) %>% 
  summarise(value = list(div.value)) %>% 
  spread(exc, value) %>% 
  group_by(div.measure) %>% 
  mutate(p_value = t.test(unlist(no), unlist(yes), paired=TRUE)$p.value, 
         t_value = t.test(unlist(no), unlist(yes), paired=TRUE)$statistic)
```


```{r Randomization trials}

####Checking out randomizations####

div.matrix.q0 = matrix(nrow=200, ncol=1000) #empty matrix for the mean differences 

for (i in 1:1000) {
  data = func.red.scale
  row.names(data) = (sample(row.names(func.red.scale)))
  a = hill_func(comm = deer.red, traits = data, q=0) %>% 
    t(.) %>% 
    as.data.frame(.) %>% 
    mutate(site = row.names(.)) %>% 
    melt(id.vars = "site", value.name = "div.value", variable.name = "div.measure") 
  div.matrix.q0[ , i] <- a$div.value
  names(div.matrix.q0)[i] = (paste("v", i, sep = ""))
}                    
#Is there a better way to do the next step? I couldn't figure it out w/in 
  #the function
div.matrix.q0 <- as.data.frame(div.matrix.q0)  %>% 
  mutate(site = a$site, div.measure = a$div.measure) %>% 
  select(site, div.measure, everything())

div.summary.q0 <- div.matrix.q0 %>% 
  mutate(mn.null = rowMeans(subset(., select = c(3:1002)))) %>% 
  mutate(sd.null = apply(subset(., select = c(3:1002)), 1, sd, na.rm = TRUE)) %>% 
  select(site, div.measure, mn.null, sd.null) %>% 
  mutate(obs = hill_q0$div.value, 
         SES = ((obs - mn.null) / sd.null)) %>% 
  separate(site, into = c("exc_name", "exc"), sep = "_", remove = FALSE)

#Boxplot
div.summary.q0 %>% 
  ggplot(aes(x=exc, y=SES))+
  geom_boxplot()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=2, outside vs. inside exclosures")

#See difference across fence
div.summary.q0 %>% 
  ggplot(aes(x=exc, y=SES, group = exc_name))+
  geom_line()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
  ggtitle("Chiu and Chao functional diversity, q=2, outside vs. inside exclosures")


#Just play with some ttests...but you are actually supposed to use
#randomization tests to compare diversity values

div.summary.q0 %>% 
  mutate(div.measure = as.character(div.measure)) %>%
  group_by(div.measure, exc) %>% 
  summarise(value = list(SES)) %>% 
  spread(exc, value) %>% 
  group_by(div.measure) %>% 
  mutate(p_value = t.test(unlist(no), unlist(yes), paired=TRUE)$p.value, 
         t_value = t.test(unlist(no), unlist(yes), paired=TRUE)$statistic)

```