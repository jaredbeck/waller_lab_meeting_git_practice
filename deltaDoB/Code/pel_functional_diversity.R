#### deltaDOB functional diversity script ####

# Things to do:
# (1) Subsample 2000s to 20 quadrats
# (2) Fix species names
# (3) Figure out how to interpret diversity metrics

#### Prepare data ####
library(dplyr)
library(reshape2)
library(tidyr)
library(ggplot2)
library(iNEXT)
# library(devtools)
# install_github("daijiang/hillR")
library(hillR)

source("deltaDoB/Code/PEL_functions.R")

# Note: the "all_clean.csv" dataset was prepared by D. Li and J.
# Ash, and was their best attempt at putting all of the PEL data 
# from all sites and time periods into a single dataset.  It is
# in long format, where each row is a single observation of a
# single species. We will use the Pine Barrens data from this
# file as a starting point.  Please remember that the data for 
# the other forest types continues to have several issues.  

#Also note: for all of all_clean, Daijiang has removed quadrats that 
#had no species

#### >> Species data ####

#Bring in the all_clean PEL data and pull out just the Pine 
#Barrens-specific data, which are referred to as CSP, 
#or central sand plains
csp = read.csv("deltaDoB/RawData/all_clean.csv") %>%
  filter(type == "CSP") %>%
  mutate(id = paste(site, date, sep = "_"),
         present = count,
         quadrat = quad,
         taxa = sp) %>%
  select(-c(site_event, site, event, quad, type, type2, date, quadrat)) %>%
  group_by(id, sp) %>%
  summarise(freq = sum(present)) %>%
  mutate(sp = as.character(sp)) 

csp.wide = csp %>%
  spread(key = sp, value = freq) %>%
  replace(is.na(.), 0) %>%
  group_by()


#### >> Functional trait data ####

#This dataset contains all spp from my exclosure dataset, whether or
#not they are in the trait dataset (made with PrepTraits.Rmd and 
#Spp.to.traits.R).  I have log transformed the highly skewed traits
#(e.g. Seed Mass, log vegetative height, etc.).  
func.pel = read.csv("deltaDoB/RawData/MeanTraitData.csv", stringsAsFactors = FALSE) %>%
  select(c(Taxon, Leaf.width, Leaf.thickness, Leaf.length, 
           Vegetative.height, Circularity, SDMC, SLA, 
           LDMC, LCC, LNC, Seed.Mass, Coef.Conservatism, 
           Pollination.mode, Dispersal.mode))
  
#### Subset functional trait and species data ####

## Find species present in both data frames
compare(func.pel$Taxon, csp$sp)$in_both
focal_species = compare(func.pel$Taxon, csp$sp)$in_both

## Subset functional trait data to those species present in both
pel.func = func.pel %>% filter(Taxon %in% focal_species)

## Subset species to those found in both
csp.data = csp.wide[,colnames(csp.wide) %in% c("id", focal_species)]


#### Functional diversity ####
rownames(pel.func) = pel.func$Taxon
pel.func = pel.func[,-1]
func.gow <- gowdis(pel.func)

csp.data = data.frame(csp.data)
colnames(csp.data) = gsub(x = colnames(csp.data), pattern = "[.]", replacement = " ")
rownames(csp.data) = csp.data$id
csp.data = csp.data[,-1]


test <- dbFD(x = as.matrix(func.gow), a = data.matrix(csp.data),
             calc.CWM = FALSE, messages = TRUE, print.pco = TRUE)
#summary(test)

csp.fdiv <- data.frame(site = c(names(test$FRic)), 
                       fric = test$FRic, 
                       fdiv = test$FDiv, 
                       fdis = test$FDis, 
                       feve = test$FEve, 
                       raoq = test$RaoQ) %>% 
  melt(id.vars = "site", value.name = "div.value", variable.name = "div.measure") %>% 
  separate(site, into = c("site", "date"), sep = "_", remove = FALSE)


#### Graph functional diversity measures ####

#Boxplot
  ggplot(csp.fdiv, aes(x=date, y=div.value))+
  geom_boxplot()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")

#See difference across fence
  ggplot(csp.fdiv, aes(x=date, y=div.value, group = site, colour = site))+
  geom_line()+
  theme_bw()+
  facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")

  
#### Chao functional diversity ####
#   - Q = (Rao's Q)
# - FDis = Functional dispersion
#          - D_q = functional hill number, the effective number of equally abundant and functionally equally distince species
#          - MD_q = mean functional diversity per species, the effective sum of pairwise distances between a fixed species and all other species
#          - FD_q = total functional diversity, the effective total functional distance between species of the assemblage


  hill_q0 <- as.data.frame(t(hill_func(comm = csp.data, traits = pel.func, q=0, div_by_sp = FALSE))) %>%
    mutate(site = row.names(.)) %>%
    melt(id.vars = "site", value.name = "div.value", variable.name = "div.measure") %>%
    separate(site, into = c("site", "date"), sep = "_", remove = FALSE)

  #Boxplot
  hill_q0 %>%
    ggplot(aes(x=date, y=div.value))+
    geom_boxplot()+
    theme_bw()+
    facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
    ggtitle("Chiu and Chao functional diversity, q=0")

  #See difference across fence
  hill_q0 %>%
    ggplot(aes(x=date, y=div.value, group = site, colour = site))+
    geom_line()+
    theme_bw()+
    facet_wrap(~div.measure, nrow = 3, ncol=2, scales = "free")+
    ggtitle("Chiu and Chao functional diversity, q=0")

  