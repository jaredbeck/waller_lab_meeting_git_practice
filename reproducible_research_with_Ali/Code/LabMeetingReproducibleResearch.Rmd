---
title: "LabMeetingReproducibleResearch"
author: "Ali Paulson"
date: "March 2, 2017"
output: html_document
---

### Introduction 

(The 3 pound signs before the word Introduction tell RMarkdown to use an increased font size...a single pound would be even bigger!)

Today in lab meeting, we are talking about reproducible research!  We will barely scratch the surface. And, if you are not familiar with R or RStudio, this may feel like a whirlwind.  

This is an RMarkdown document.  It is cool becuase I can use the Knit button, and it will run my code and produce an html (or PDF or .doc) document that I can share with Don or other collaborators.  This is especially cool if you have a series of plots or results that you want to share with a collaborator.  You can just do it within your RMarkdown document, you don't need to copy and paste a billion different plots in an outside editor.  

You don't have to use R Markdown though - in fact, if you are just cleaning your data, and don't have output to share, then it may be better to just use a simple R code.  

Here's the list of things that we will cover very briefly today: 

1. Quick run down of ideas behind reproducible research
2. Resources for reproducible research/data wrangling in R
3. dplyr - an amazing package that will change your life, with a VERY brief intro today
4. We'll touch on a couple other packages as well: tidyr, reshape2, and ggplot2.
5. git

(I made a numbered list above...that will be formatted nicely as a list in the html output)

These things take practice, so don't worry if you get a little bit of whiplash.  Jared and I (and probably several others) can serve as resources for you if you are trying to use any of these tools in the future.  

###Ideas behind reproducible research

Much of this information comes from [Karl Broman](kbroman.org), a UW-Madison Professor in the Department of Biostatistics and Medical Informatics.  On his website, he has a great section about [getting started with reproducible research](http://kbroman.org/steps2rr/). I recommend that you check it out!  

(Did you see how I made those hyperlinks above?)

Reproducible = Your code and data are assembled so that another group can recreate all of the results
 - reproducible != correct/mistake free
 - there are a lot of tools out there - don't try to learn and implement all of them at once...look for a tool that you can integrate now, and try to implement something new with each project
 - "Your closest collaborator is you six months ago, but you don't reply to emails" (Mark Holder)
 
Karl Broman talks about how the basis for any reproducible research is good organization

1. Everything for one project should be in a single directory (all data, code, results)
     i) This way, you can find things more easily
     ii) You can also transfer your project to other collaborators more easily
2. Use specific subfolders in your project directory
     i) e.g. RawData, CleanData, Code, Figures, Reports
     ii) You will know which types of files are in each subdirectory
     iii) Will allow you to separate your data from your code
3. Within your R Code (in your project directory), use relative paths, not absolute paths so that you can transfer your R code to other collaborators without them needing to change their path
4. Choose your file names carefully
     i) don't change raw data names from collaborators (you'll want to be able to trace these)
     ii) But the code and clean data that you create should have clear and explicit names
         A.  No spaces
         B.  Human readable name
         C.  Specific ("CleanData.csv" isn't great because it's not specific to a given project..."ExclosureClean.csv" would be better)
         D.  CamelCase is good
         E.  Lots of other recommendations online for this - you can follow [Hadley Wickham's style guide for R](http://adv-r.had.co.nz/Style.html) - this might be best. 
     iii) Don't every use "final" in a file name...that's just asking for revisions
5. Include a ReadMe file with your directory that describes all of your files, codes, and processes.  You should update this when you update your project.  This will save you in the future, especially when you have a large project. 
6. Start your project with the rawest data possible!  
     i) Don't hand edit your data files. If there is an error, you should correct it with a script, so that you have a record of that error and the correction. 
     ii) Complete all data cleaning with a script (there are other programs besides R for data cleaning, like OpenRefine, but you can do a lot in R!). 
7. Conduct all of your analyses using scripts
8. Conduct all analyses using sequential scripts (ie: you should be able to run a code from start to finish and have it work...you shouldn't need to hop around)

####Some additional notes from Ali: 
1. Write small scripts for each step, especially your data cleaning.  
     i) This allows you to clean your R environment...especially important if you are using large datasets
     ii) You can clean your data in one script, save your clean data as a new file, then just bring your clean data into other code scripts.  Why? You don't want to reclean your data in every script that you use it.  
2. If you will be repeating similar steps for many species, or many sites (e.g. National Parks), write a function (rather than copy, paste, find, replace).  It might take you a bit of time to write the function, but it will likely save you time and hassle in the long run. 
3. When recording data after your field season, write your data in long format (each row is a single observation). 



###Resources: 

**For Reproducible Research:**

1. [Getting started with reproducible research](http://kbroman.org/steps2rr/): Karl Broman's website with more detail re: steps to reproducible research
2. The data carpentry workshop that Ali went to: <https://uw-madison-aci.github.io/2016-08-23-uwmadison/>
3. The software carpentry workshop that Ali went to: <https://uw-madison-aci.github.io/2017-01-12-uwmadison/>
4. Mailing list for the [UW-Madison Advanced Computing Initiative](https://aci.wisc.edu) (so you can sign up for one of these workshops too!): <http://wisc.us9.list-manage.com/subscribe?u=c3aae71670855b66d79d170b8&id=6d65510fb6>

**For Data Wrangling (Data Cleaning)**

1. [dplyr](https://cran.r-project.org/web/packages/dplyr/dplyr.pdf)
    i) [Really good description of the power of dplyr and how to use it](https://cran.rstudio.com/web/packages/dplyr/vignettes/introduction.html)
    ii) [dplyr cheatsheet](https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf)

2. [reshape2](https://cran.r-project.org/web/packages/reshape2/reshape2.pdf): for changing your data structure from wide to long format (this is how you make a site x species table!)

3. [tidyr](https://cran.r-project.org/web/packages/tidyr/tidyr.pdf): This package helps you tidy your data (you can remove extra spaces, you can combine and separate variables, you can replace parts of a variable...often useful for cleaning tasks that are otherwise complicated)
     i) [Information from Hadley Wickham re: tidyr](ftp://cran.r-project.org/pub/R/web/packages/tidyr/vignettes/tidy-data.html)

4. [ggplot2](https://cran.r-project.org/web/packages/ggplot2/ggplot2.pdf): An amazing package for making figures.  Takes a little time to figure out, but it's far better than base R, once you figure it out.  And, it integrates directly with dplyr, which is awesome.  
     i) [R Graph gallery](http://www.r-graph-gallery.com/portfolio/ggplot2-package/) Examples of many different figure types, and you can get the code behind the different figure types
     ii) [ggplot2 Cheat Sheet](https://www.rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf)

5. [RMarkdown](http://rmarkdown.rstudio.com): Allows you to produce output that can easily be passed between collaborators. This is an R Markdown Document
     i) [RMarkdown Cheat Sheet](https://www.rstudio.com/wp-content/uploads/2016/03/rmarkdown-cheatsheet-2.0.pdf)

6. [RStudio](https://www.rstudio.com): Integrated Development Environment.  If you use R, then you will love RStudio.  It allows you to see your project, your environment, to write scripts, and to run an R console all in one window.  
     i) [RStudio Cheat Sheet](https://www.rstudio.com/wp-content/uploads/2016/01/rstudio-IDE-cheatsheet.pdf)
     
**Note that all of the packages above were created by Hadley Wickham.  When you write a paper, you'll want to be sure to cite the developers of the R programs that you use!  It's just like citing articles in your field.**


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE) #echo=TRUE means that the R code will be included in the output.  Most of the time you don't want this - ie: Don doesn't need to see all of my code, he's more interested in the output.  You can say echo=FALSE to not include your code in the output.  There are all sorts of other RMarkdown options - see the RMarkdown Cheat Sheet
```


First, I will bring in the PEL data

```{r PEL data}
#Inside these R chunks, I need to use a pound sign to denote a comment.  These lines of text won't be read as code. 

pel.raw <- read.csv("../RawData/pel_all_clean.csv", stringsAsFactors = FALSE)
View(pel.raw)
str(pel.raw)

unique(pel.raw$type)

unique(pel.raw$site)
#I don't want site to have those decimals, so I will change site to be an integer, rather than numeric
pel.raw$site <- as.integer(pel.raw$site)
```

Next, let's play around with dplyr.  dplyr can do a lot of really cool things (see the references I gave you above).  I will just show a few neat examples here.  %>% is a piping operator, it takes the output of the command before it as input.  Because of this, you can chain many commands together.  

```{r dplyr}

#Let's play with dplyr
#install.packages("dplyr") #Install the package (you'll need to uncomment this if you don't have it already)
library(dplyr) #Load the package

#What if I just want the data from the Southern Upland Forests (SUF)?
suf <- pel.raw %>% 
  filter(type == "SUF")
#If you look at the dataframe suf, you can see that only the Southern Upland Forests are included

#Now, what if I only wanted the data from the 2000s sampling period, not the 1950's? 
suf <- suf %>% 
  filter(date == "2000s") #Now, the size of suf has been reduced, and we only have the 2000's era surveys

#You can chain these two commands together in a couple of different ways: 

suf.b <- pel.raw %>% 
  filter(type == "SUF") %>% 
  filter(date == "2000s")

#or 

suf.c <- pel.raw %>% 
  filter(type=="SUF", date=="2000s")

#all of these dataframes are the same! 


#You can also use dplyr to arrange your data (which is basically sorting)
suf.arrange <- suf %>% 
  arrange(sp) 

#This will put the species in alphabetical order.  There are lots of other options with arrange as well.  


#What if I only care about certain columns?  For instance, I only want to know the species, count and site - the rest of the information is extraneous to me. I can use the "select" command

suf.select <- suf %>% 
  select(site, sp, count)


#Now, what if I wanted to know how may unique species were found in the SUF during the 2000s?

suf.species <- suf %>% 
  distinct(sp)

#What if I want to know how many times each species occurs? This will take some chaining

suf.spp.occ <- suf %>% 
  group_by(sp) %>% 
  summarise(count = n())


#What if I want to know how many times each species occurs at each site? This is important for making our sitexspecies table below. 

suf.spp.occ.bysite <- suf %>% 
  group_by(site, sp) %>% 
  summarise(count = n())
```


There are a ton of other cool things that you can do with dplyr - you can use mutate to add a new column to a data frame (perhaps the sum or average of other columns), you can do a random sample of rows using sample_n().  The real power comes in when you start being able to group your dataset in different ways using piping, without creating a whole bunch of intermediate dataframes.  Also, you can pipe directly into ggplot2, which is a package for making figures.  We won't get into ggplot2 today.  


Another common thing that people need to do in our line of work is to change a dataframe from long format to wide format (ie: site x species table).  To do this, you can use the melt() and cast() commands from reshape2.  

```{r reshape2}
#install.packages("reshape2")
library(reshape2)
#Let's say we start with the southern upland forests data from the 2000s. We created a dataframe above that just has the site, species, and count (it's called suf.select)

str(suf.select)

suf.sitebyspecies <- dcast(suf.spp.occ.bysite, site ~ sp, drop=FALSE)

suf.sitebyspecies[is.na(suf.sitebyspecies)]=0

#You can also use melt to go backwards from wide to long frame
```


That's a very brief intro to dplyr and reshape2.  These packages are very powerful, more so than we have time to go into.  You'll also appreciate tidyr for solving common data cleaning problems.  

I'm leaving the basic RMarkdown plot down below, just so that you can see what a plot would look like coming from RMarkdown.  We'll save ggplot2 for another day.  




## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```
